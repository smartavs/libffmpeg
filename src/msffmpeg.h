#ifndef MSFFMPEG_H
#define MSFFMPEG_H
#include <libmsffmpeg/ffmpeg.h>
#ifndef MSFFMPEG_C
extern void 
	msffmepg_api_envinit();
extern void 
	msffmepg_api_envdeinit();
extern FFMPEGContext *
	msffmepg_api_init(FFMPEGContext *pffmpeg_ctt_in,
		char (*control_cbfunc)(void *control_cbfunc_arg),void *control_cbfunc_arg,
		char (*bitrate_cbfunc)(void *bitrate_cbfunc_arg,int size,char flag_recv),void *bitrate_cbfunc_arg,
		int (*detect_cbfunc)(void * detect_cbfunc_arg,AVFrame *filt_frame,AVFrame *frame_dec,STREAMCODECContext *pscodec_ctt),void * detect_cbfunc_arg);
extern void 
	msffmepg_api_deinit(FFMPEGContext **ppffmpeg_ctt);
extern int 
	msffmepg_api_runcmd(int argc,char** argv,FFMPEGContext *pffmpeg_ctt);
extern int 
	msffmepg_api_run(char * cmdline,FFMPEGContext *pffmpeg_ctt);
extern char 
	msffmepg_api_taskstop(FFMPEGContext *pffmpeg_ctt,char flag_waitstop);
extern char * 
	msffmepg_api_version();
#endif
#endif