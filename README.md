# libffmpeg

### 1.介绍

关键字：ffmpeg多线程，ffmpeg多进程

项目是基于开源项目中的fftools模块进行移植和修改，主要目的

- 接口参数使用命令行字符串的方式，减小命令行到API接口过渡的学习成本；
- 支持FFMPEG的API级别的多线程调用；
- 支持实时获取码率信息和帧信息；
- 支持授权或指令控制；
- 支持FFMPEG命令行所有功能；
- 通过回调机制，具备良好的横向业务扩展能力；

注意：ffmpeg相关库是基于 [修改过的ffmpeg项目](https://gitee.com/smartavs/ffmpeg.git  )  编译的，并且也是基于该版本进行的测试和验证。项目第一个版本基于FFMPEG-N4.4.3，最新基于FFMPEG-N6.1.1;

### 2.编译说明

#### 2.1.源码下载

```
git clone https://gitee.com/smartavs/ffmpeg.git
git clone https://gitee.com/smartavs/libffmpeg.git
```

#### 2.2.FFMPEG库编译

- 进入ffmpeg源码路径，创建脚本文件usr_complie.sh；

- 脚本文件usr_complie.sh内加入以下内容；

```
#! /bin/bash
prefix=/usr/local/mscore
export PKG_CONFIG_PATH=${prefix}/lib/pkgconfig:$PKG_CONFIG_PATH

##ffmpeg uses
echo "Install libs(sdl,sdl2,fontconfig,fribidi,yasm) for ffmpeg"
apt install -y  libsdl1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsdl-gfx1.2-dev libsdl2-* *fontconfig* *fribidi* yasm

bash configure \
    --prefix=${prefix} \
    --extra-ldflags=-L${prefix}/lib \
    --extra-cflags=-I${prefix}/include \
    --enable-shared \
    --disable-static \
    --enable-x86asm \
    --disable-doc \
    --enable-gpl \
    --enable-pthreads

make && make install
```

- 执行脚本文件usr_complie.sh，完成编译；

#### 2.3.LIBFFMPEG库编译

- 进入libffmpeg源码路径，然后执行指令make进行编译和安装；
- 安装路径默认在/usr/local/mscore，可以通过修改Makefile文件进行配置（prefix_x8664=/usr/local/mscore）；

#### 2.4.LIBFFMPEG库样例编译和运行
- 进入libffmpeg源码路径，然后执行指令make sample进行样例的编译；
- 执行./out/bin/msffmpeg_api_sample进行功能测试；

### 3.使用说明

**3.1.全局环境初始化和清理函数**

ffmpeg库的初始化和清理函数，整个应用程序只需要开始调用一次msffmepg_api_envinit进行初始化，应用程序退出时释放一次即可；

```
extern int msffmepg_api_envinit();
extern int msffmepg_api_envdeinit();
```

**3.2.始化函数**

函数功能说明：用于分配并初始化FFMPEGCONText

传入参数说明：

​         <1>char (*control_cbfunc)(void *control_cbfunc_arg)用于进行执行控制的回调函数，如授权控制，退出指令控制等。当回调函数返回0时，任务线程将会退出。如果不需要控制，则传入NULL；

​         <2>control_cbfunc_arg用于传入回调函数需要的程序变量结构体。如果不需要，则传入NULL；

​         <3>char (*bitrate_cbfunc)(void *bitrate_cbfunc_arg,int size,char flag_recv)用于获取接收和发送码率信息和运行时间等的回调函数，size传入本次回调的包大小，flag_recv表明当前回调是接收还是发送。如果不需要计算码率，则传入NULL；

​         <4>bitrate_cbfunc_arg用于传入回调函数需要的程序变量结构体。如果不需要，则传入NULL；

​         <5>int (*detect_cbfunc)(void * detect_cbfunc_arg,AVFrame *filt_frame,AVFrame *frame_dec,STREAMCODECContext *pscodec_ctt)用于帧信息的检测和分析

​         <6>detect_cbfunc_arg用于传入回调函数需要的程序变量结构体。如果不需要，则传入NULL；

返回参数说明：失败返回NULL，成功则返回分配成功的FFMPEGCONText指针；

```
FFMPEGCONText *msffmepg_api_init(FFMPEGCONText *pffmpeg_ctt_in,
	char (*control_cbfunc)(void *control_cbfunc_arg),void *control_cbfunc_arg,
	char (*bitrate_cbfunc)(void *bitrate_cbfunc_arg,int size,char flag_recv),void *bitrate_cbfunc_arg,
	int (*detect_cbfunc)(void * detect_cbfunc_arg,AVFrame *filt_frame,AVFrame *frame_dec,STREAMCODECContext *pscodec_ctt),void * detect_cbfunc_arg)
```
**3.3.清理函数**

函数功能说明：释放FFMPEGCONText空间

```
extern void msffmepg_api_deinit(FFMPEGCONText **ppffmpeg_ctt);
```
**3.4.执行函数**

函数功能说明：实际的命令执行函数，和msffmepg_api_run功能一样，传入参数有差异

传入参数说明：

​         <1>argc表示传入的参数个数，如ffmpeg  -i  input.mp3  output.ts，argc传入4；

​         <2>argv表示传入的具体参数，如“ffmpeg  -i  input.mp3  output.ts”；

​         <3>pffmpeg_ctt为通过接口msffmepg_api_init函数获取到的指针；

返回参数说明：无实际意义；

```
extern int msffmepg_api_runcmd(int argc,char** argv,FFMPEGCONText *pffmpeg_ctt);
```
**3.5.执行函数2**

函数功能说明：实际的命令执行函数，和msffmepg_api_runcmd功能一样，传入参数有差异

传入参数说明：

​         <1>cmdline命令行字符串，如ffmpeg  -i  input.mp3  output.ts，argc

​         <2>pffmpeg_ctt为通过接口msffmepg_api_init获取到的指针；

返回参数说明：无实际意义；

```
extern int msffmepg_api_run(char * cmdline,FFMPEGCONText *pffmpeg_ctt);
```
**3.6.命令停止函数**

传入参数说明：

​         <1>pffmpeg_ctt为通过接口msffmepg_api_init获取到的指针；

​         <2>flag_waitstop指明是否等待执行退出：1表示同步执行，阻塞等待执行退出或30秒超时；0表示异步执行；

返回参数说明：0表示停止成功；-1表示停止失败；1表示pffmpeg_ctt为空，停止失败；

```
extern char msffmepg_api_taskstop(FFMPEGCONText *pffmpeg_ctt,char flag_waitstop);
```

