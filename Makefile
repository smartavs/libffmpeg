#####custom
prefix_aarch64=/usr/local/mscore_aarch64
include platform/aarch64_linux.mk
prefix_armv7l=/usr/local/mscore_armv7l
include platform/armv7l_linux.mk
prefix_x8664=/usr/local/mscore
include platform/x8664_linux.mk
export PKG_CONFIG_PATH:=${prefix_x8664}/lib/pkgconfig:${prefix_armv7l}/lib/pkgconfig:${prefix_aarch64}/lib/pkgconfig:$PKG_CONFIG_PATH
#########
PROGRAM_NAME=libmsffmpeg

OUT_LIB=out/lib
OUT_BIN=out/bin
OUT_INCLUDE=out/include/$(PROGRAM_NAME)

CFLAGS_SHARE=-shared
C_SRC= \
	src/cmdutils.c \
	src/ffmpeg_dec.c \
	src/ffmpeg_demux.c \
	src/ffmpeg_enc.c \
	src/ffmpeg_filter.c \
	src/ffmpeg_hw.c \
	src/ffmpeg_mux_init.c \
	src/ffmpeg_mux.c \
	src/ffmpeg_opt.c \
	src/ffmpeg.c \
	src/objpool.c \
	src/opt_common.c \
	src/sync_queue.c \
	src/thread_queue.c 
	
all: x8664

test: x8664 


clean:
	rm  out -fr
help:
	@echo "USAGE:make target"
	@echo "    x8664		Produce the ${PROGRAM_NAME}.so,and install it"
	@echo "    sample     		Sample program with ${PROGRAM_NAME} api"
