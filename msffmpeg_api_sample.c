#include "libmsffmpeg/msffmpeg.h"

char control_cbfunc(void *pcontrol_cbfunc_arg)
{
	return 1;
}
char bitrate_cbfunc(void *bitrate_cbfunc_arg,int size,char flag_recv)
{
	return 0;
}
int detect_cbfunc(void * detect_cbfunc_arg,AVFrame *filt_frame,AVFrame *frame_dec,STREAMCODECContext *pscodec_ctt)
{
	return 0;
}
int main(int argc, char **argv)
{
	//可用于测试FFMPEG是否被更新
	char cmdline[5120]={0};
	sprintf(cmdline, "%s  -stream_loop  -1 -i  "
		"udp://127.0.0.1:12036 -c:a mp3  -b:a 64000 -c:v copy   -metadata service_name=MSCORE   -metadata service_provider=MSAVSKIT "
		"-mpegts_service_id 65281   -mpegts_transport_stream_id 1   -mpegts_original_network_id 1   -mpegts_pmt_start_pid %d   -mpegts_start_pid %d  "
		"-metadata encoder=mscore_encoder   -f mpegts udp://127.0.0.1:6000?pkt_size=1316\n", 
		argv[0],0X12,0X1FFB);
	if(argc<2){
		printf( "uage: %s \n", cmdline);
		return -1;
	}
	msffmepg_api_envinit();

	char control_cbfunc_arg;
	char bitrate_cbfunc_arg;
	char detect_cbfunc_arg;
	FFMPEGContext *pffmpeg_ctt=msffmepg_api_init(NULL,
		control_cbfunc,&control_cbfunc_arg,
		bitrate_cbfunc,&bitrate_cbfunc_arg,
		detect_cbfunc,&detect_cbfunc_arg);
	
	int ret=0;
	if(!strncmp(argv[1],"test",strlen("test"))){
		ret=msffmepg_api_run(cmdline, pffmpeg_ctt);
	}else{
		ret=msffmepg_api_runcmd( argc, argv, pffmpeg_ctt);
	}
	printf( "msffmepg_api_runcmd,ret=%d\n", ret);
	msffmepg_api_deinit(&pffmpeg_ctt);
	msffmepg_api_envdeinit();
}