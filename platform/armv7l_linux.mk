D_ARMV7L_FFMPEG_LIBS= \
	`pkg-config --libs libavcodec_armv7l` \
	`pkg-config --libs libavfilter_armv7l` \
	`pkg-config --libs libavformat_armv7l` \
	`pkg-config --libs libavutil_armv7l` \
	`pkg-config --libs libswresample_armv7l` \
	`pkg-config --libs libswscale_armv7l`
D_ARMV7L_FFMPEG_INCLUDES= \
	`pkg-config --cflags libavcodec_armv7l` \
	`pkg-config --cflags libavfilter_armv7l` \
	`pkg-config --cflags libavformat_armv7l` \
	`pkg-config --cflags libavutil_armv7l` \
	`pkg-config --cflags libswresample_armv7l` \
	`pkg-config --cflags libswscale_armv7l`

libmsffmpeg_armv7l_PATH=/home/sugao/H2_toolchain/toolchain/4.9.3
libmsffmpeg_armv7l_CC=${libmsffmpeg_armv7l_PATH}/bin/arm-cortexa9-linux-gnueabihf-gcc
libmsffmpeg_armv7l_SRC=${C_SRC}
libmsffmpeg_armv7l_CFLAGS=${D_ARMV7L_FFMPEG_INCLUDES} ${D_ARMV7L_FFMPEG_LIBS} ${CFLAGS_SHARE} -fPIC  -I../ffmpeg/ -std=gnu11

armv7l: libmsffmpeg_armv7l install_armv7l
libmsffmpeg_armv7l: 
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS) -o $(OUT_LIB)/$(PROGRAM_NAME).so 
install_armv7l:
	mkdir -p  ${prefix_armv7l}/lib/ 
	mkdir -p  ${prefix_armv7l}/include/   
	mkdir -p  ${prefix_armv7l}/lib/pkgconfig/ 
	cp -fr $(OUT_LIB)/$(PROGRAM_NAME).so		${prefix_armv7l}/lib/     
	cp -fr $(OUT_INCLUDE)				${prefix_armv7l}/include/	     
	cp -fr platform/$(PROGRAM_NAME)_armv7l.pc	${prefix_armv7l}/lib/pkgconfig/
uninstall_armv7l:
	rm -fr ${prefix_armv7l}/lib/$(PROGRAM_NAME).so 
	rm -fr ${prefix_armv7l}/include/$(PROGRAM_NAME)
	rm -fr ${prefix_armv7l}/lib/pkgconfig/$(PROGRAM_NAME).pc
