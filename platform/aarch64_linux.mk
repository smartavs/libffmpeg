D_AARCH64_FFMPEG_LIBS= \
	`pkg-config --libs libavcodec_aarch64` \
	`pkg-config --libs libavfilter_aarch64` \
	`pkg-config --libs libavformat_aarch64` \
	`pkg-config --libs libavutil_aarch64` \
	`pkg-config --libs libswresample_aarch64` \
	`pkg-config --libs libswscale_aarch64`
D_AARCH64_FFMPEG_INCLUDES= \
	`pkg-config --cflags libavcodec_aarch64` \
	`pkg-config --cflags libavfilter_aarch64` \
	`pkg-config --cflags libavformat_aarch64` \
	`pkg-config --cflags libavutil_aarch64` \
	`pkg-config --cflags libswresample_aarch64` \
	`pkg-config --cflags libswscale_aarch64`

libmsffmpeg_aarch64_CC=aarch64-linux-gnu-gcc
libmsffmpeg_aarch64_SRC=${C_SRC}
libmsffmpeg_aarch64_CFLAGS=${D_AARCH64_FFMPEG_INCLUDES} ${D_AARCH64_FFMPEG_LIBS} ${CFLAGS_SHARE} -fPIC  -I../ffmpeg/ -std=gnu11

aarch64: libmsffmpeg_aarch64 install_aarch64
libmsffmpeg_aarch64: 
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS) -o $(OUT_LIB)/$(PROGRAM_NAME).so 
install_aarch64:
	mkdir -p  ${prefix_aarch64}/lib/ 
	mkdir -p  ${prefix_aarch64}/include/   
	mkdir -p  ${prefix_aarch64}/lib/pkgconfig/ 
	cp -fr $(OUT_LIB)/$(PROGRAM_NAME).so		${prefix_aarch64}/lib/     
	cp -fr $(OUT_INCLUDE)				${prefix_aarch64}/include/	     
	cp -fr platform/$(PROGRAM_NAME)_aarch64.pc	${prefix_aarch64}/lib/pkgconfig/
uninstall_aarch64:
	rm -fr ${prefix_aarch64}/lib/$(PROGRAM_NAME).so 
	rm -fr ${prefix_aarch64}/include/$(PROGRAM_NAME)
	rm -fr ${prefix_aarch64}/lib/pkgconfig/$(PROGRAM_NAME).pc
