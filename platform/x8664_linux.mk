D_LIBMSFFMPEG_LIBS		:=`pkg-config --libs	libmsffmpeg`
D_LIBMSFFMPEG_INCLUDES	:=`pkg-config --cflags	libmsffmpeg`
D_FFMPEG_LIBS=\
	`pkg-config --libs libavcodec` \
	`pkg-config --libs libavfilter` \
	`pkg-config --libs libavformat` \
	`pkg-config --libs libavutil` \
	`pkg-config --libs libavdevice` \
	`pkg-config --libs libswresample` \
	`pkg-config --libs libpostproc` \
	`pkg-config --libs libswscale`
D_FFMPEG_INCLUDES= \
	`pkg-config --cflags libavcodec` \
	`pkg-config --cflags libavfilter` \
	`pkg-config --cflags libavformat` \
	`pkg-config --cflags libavutil` \
	`pkg-config --cflags libavdevice` \
	`pkg-config --cflags libswresample` \
	`pkg-config --cflags libpostproc` \
	`pkg-config --cflags libswscale`

libmsffmpeg_CC=${CC}
libmsffmpeg_SRC=${C_SRC}
libmsffmpeg_CFLAGS=${D_FFMPEG_INCLUDES} ${D_FFMPEG_LIBS} ${CFLAGS_SHARE} -fPIC  -I../ffmpeg/ -Wl,-Bsymbolic

msffmpeg_api_sample_CC=${CC}
msffmpeg_api_sample_SRC=msffmpeg_api_sample.c
msffmpeg_api_sample_CFLAGS=${D_LIBMSFFMPEG_INCLUDES} ${D_LIBMSFFMPEG_LIBS} -Isrc 

x8664: libmsffmpeg install

simple: msffmpeg_api_sample install
	
libmsffmpeg: 
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS)  -o $(OUT_LIB)/$(PROGRAM_NAME).so 
gdbs:
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$(libmsffmpeg_CC)  $(libmsffmpeg_SRC)  $(libmsffmpeg_CFLAGS) -g -o $(OUT_LIB)/$(PROGRAM_NAME).so  

sample: msffmpeg_api_sample 

msffmpeg_api_sample: 
	mkdir -p $(OUT_BIN)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS)  -o $(OUT_BIN)/$@ 
install:
	mkdir -p  ${prefix_x8664}/lib/ 
	mkdir -p  ${prefix_x8664}/include/   
	mkdir -p  ${prefix_x8664}/lib/pkgconfig/ 
	cp -fr $(OUT_LIB)/$(PROGRAM_NAME).so	${prefix_x8664}/lib/     
	cp -fr $(OUT_INCLUDE)			${prefix_x8664}/include/	     
	cp -fr platform/$(PROGRAM_NAME).pc	${prefix_x8664}/lib/pkgconfig/
install_gdb:
	mkdir -p  ${prefix_x8664}/lib_gdb/ 
	mkdir -p  ${prefix_x8664}/include/   
	mkdir -p  ${prefix_x8664}/lib/pkgconfig/ 
	cp -fr $(OUT_LIB)/$(PROGRAM_NAME).so	${prefix_x8664}/lib_gdb/     
	cp -fr $(OUT_INCLUDE)			${prefix_x8664}/include/	     
	cp -fr platform/$(PROGRAM_NAME).pc	${prefix_x8664}/lib/pkgconfig/
uninstall:
	rm -fr ${prefix_x8664}/lib/$(PROGRAM_NAME).so 
	rm -fr ${prefix_x8664}/include/$(PROGRAM_NAME)
	rm -fr ${prefix_x8664}/lib/pkgconfig/$(PROGRAM_NAME).pc
